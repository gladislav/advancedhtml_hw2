const burger = document.querySelector('.burger-span')
burger.addEventListener('click', () => {
  burger.classList.toggle('burger-span-close')
  document.querySelector('.burger-list').classList.toggle('hide')
})
